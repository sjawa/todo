class TasksController < ApplicationController
  def new
    @task = Task.new
    render :show_form
  end

  def create
    @task = Task.create(task_params)
    @tasks = Task.all
    save_task
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy
    @tasks = Task.all
  end

  def update
    @task = Task.find(params[:id])
    @task.update_attributes(task_params)
    save_task
    #render :hide_form
  end

  def edit
    @task = Task.find(params[:id])
    render :show_form
  end

  def show
  end

  private

  def task_params
    params.require(:task).permit(:title, :note, :completed)
  end

  def save_task
    if @task.save
      @tasks = Task.all
      render :hide_form
    else
      render :show_form
    end
  end

end
